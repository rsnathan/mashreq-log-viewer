const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('./db.json');
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 3000;
var jwt = require('jsonwebtoken');

var bodyParser = require('body-parser')


server.use(bodyParser.urlencoded());
server.use(bodyParser.json());


// Add headers
server.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

server.get('/logs/count', function (req, res) {
    let logs = router.db.get('logs').value()
    let _res = [];
    let data = {};
    let simplieddate = [];
    for (let i = 0; i < logs.length; i++) {
        let logdate = logs[i].time.split(":")[0];
        logdate = new Date(logdate);
        if (new Date(req.query.startdate) <= logdate && new Date(req.query.enddate) >= logdate) {
            simplieddate.push(logs[i]);
        }
    }

    data.msg = "Count Successfully";
    data.success = true;
    data.data = simplieddate.length;

    return res.json(data)
})

server.get('/logs/list', function (req, res) {
    let logs = router.db.get('logs').value()
    let _res = [];
    let data = {};

    let simplieddate = [];
    for (let i = 0; i < logs.length; i++) {
        let logdate = logs[i].time.split(":")[0];
        logdate = new Date(logdate);
        if (new Date(req.query.startdate) <= logdate && new Date(req.query.enddate) >= logdate) {
            simplieddate.push(logs[i]);
        }
    }
    for (let i = req.query.offset; i < (parseInt(req.query.limit) + parseInt(req.query.offset)); i++) {
        _res.push(simplieddate[i]);
    }
    data.msg = "Listed Successfully";
    data.success = true;
    data.data = _res;

    return res.json(data)
})

server.get('/user', function (req, res) {
    const token = req.headers['authorization']
    if (!token) return res.status(401).json('Unauthorize user')

    jwt.verify(token, 'admin123', function (err, decoded) {
        if (err) {
            return res.status(401).json({
                data: [],
                message: err.message,
                success: false
            })
        }
        
        return res.json({
            ...decoded,
        })
    });



})

server.post('/auth/login', function (req, res) {
    let { user } = req.body;
    let users = router.db.get('users').value()
    let data = {};
    for (let i = 0; i < users.length; i++) {
        if (users[i].email == user.email) {
            if (users[i].password == user.password) {
                data.msg = "User successfully logined to the syetem";
                data.success = true;
                data.jwt = jwt.sign({
                    email: users[i].email, name: users[i].name, id: users[i].userId
                }, 'admin123',
                    { expiresIn: '1h' }
                );
                data.name = users[i].email;

            } else {
                data.msg = "Username or Password Incorrenct";
                data.success = false;
                data.data = []
            }

        } else {
            data.msg = "User couln't found in the database";
            data.success = false;
            data.data = []
        }
        console.log(data)
        return res.json(data)
    }
})

server.use(middlewares);
server.use(router);
server.listen(port, () => {
    console.log(`App Running PORT ${port}`)
});