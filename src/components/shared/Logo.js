import React from 'react';


class Logo extends React.Component {
    render() {
        return (
            <div className={this.props.className || "logo-container"}>
                <img src="/assets/img/logo.png" />
            </div>
        );
    }
}

export default Logo;
