import React from 'react';
import ListErrors from '../ListErrors';
import { DatePicker } from 'react-nice-dates'
import 'react-nice-dates/build/style.css'
import { enGB } from 'date-fns/locale'


class Input extends React.Component {

    changeDate = (e, name) => {
        let data = {
            target: {
                name: name,
                value: new Date(e)
            }
        }
        this.props.onChange(data)
    }
    render() {
        console.log(this.props)
        return (
            <div className={"material-input-wrapper-vimbo " + (this.props.inputError ? 'has-error' : '')}>
                {this.props.type == 'date' && <DatePicker name={this.props.name}
                    selected={this.props.value}
                    date={new Date(this.props.value)}
                    onDateChange={(e) => this.changeDate(e, this.props.name)}
                    locale={enGB}

                >
                    {({ inputProps, focused }) => (
                        <input
                            className={'input material-input-extra-vimbo' + (focused ? ' -focused' : '')}
                            {...inputProps}
                        />
                    )}
                </DatePicker>

                }
                {this.props.type != 'date' && <input name={this.props.name} onChange={this.props.onChange} className="material-input-extra-vimbo" placeholder={this.props.placeholder} type={this.props.type} required value={this.props.value} />}
                {/* <label className="material-label-extra-vimbo">{this.props.label} {this.props.inputError}</label> */}
                {this.props.inputError && <ListErrors errors={this.props.errorMsg} />}
            </div>
        );
    }
}

export default Input;
