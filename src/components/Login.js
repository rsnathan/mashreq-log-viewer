import { Link } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {
  UPDATE_FIELD_AUTH,
  LOGIN,
  LOGIN_PAGE_UNLOADED
} from '../constants/actionTypes';

import Input from './shared/input';
import Logo from './shared/Logo';

const mapStateToProps = state => ({ ...state.auth });

const mapDispatchToProps = dispatch => ({
  onSubmit: (data) =>
    dispatch({ type: LOGIN, payload: agent.Auth.login(data) }),
  onUnload: () =>
    dispatch({ type: LOGIN_PAGE_UNLOADED })
});

class Login extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.state = {
      login: {
        email: { valid: false, _value: "", touched: false, required: true, msg: "Username is required." },
        password: { valid: false, _value: "", touched: false, required: true, msg: "Password is required." },
      },
      submitted: false
    }
  }

  ValueExatractor = (data) => {
    let res = {};
    let _k = "_value";
    for (let key in data) {
      res[key] = [];
      for (let _key in data[key]) {
        if (_key == _k) {
          res[key] = data[key][_key]
        }

      }
    }
    return res;
  }

  submitForm = (email, password) => ev => {
    ev.preventDefault();
    this.setState({
      submitted: true
    })
    console.log(this.handleValidations());
    if (!this.handleValidations()) {
      let data = this.ValueExatractor(this.state.login);
      this.props.onSubmit(data)
    }
  };

  handleValidations = () => {
    let login = this.state.login;
    let error = false;
    for (var i in login) {
      if (login[i].required && !login[i]._value) {
        error = true;
      }
    }
    return error;
  }

  componentDidMount() {
    const token = window.localStorage.getItem('jwt');
    // if (token) {
    //   this.props.history.replace('/home')
    // }
  }

  componentDidUpdate(prevProps) {
    if (this.props.loginsuccess) {
      this.props.history.replace('/home');
    }
  }

  onHandleInput = (e) => {
    const { name, value } = e.target;
    this.setState(prevState => {
      let login = Object.assign({}, this.state.login);
      login[name]._value = value;
      login[name].touched = true;
      return { login };
    })
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { email, password } = this.state.login;
    return (

      <div className="auth-page">
        <div className="row-header">
          <div className="col-md-4 offset-md-4 col-xs-12">
            <h1 className="text-xs-center">Sign In</h1>
            <Logo />
            <form className="material-form-extra-vimbo " noValidate onSubmit={this.submitForm()}>
              <Input
                type="text"
                placeholder="Email"
                label="Email"
                value={email._value}
                name={'email'}
                onChange={this.onHandleInput}
                errorMsg={this.state.login.email.msg}
                inputError={(this.state.submitted || this.state.login.email.touched) && !this.state.login.email._value}
              />
              <Input
                type="password"
                placeholder="Password"
                label="Password"
                value={password._value}
                name={'password'}
                onChange={this.onHandleInput}
                errorMsg={this.state.login.password.msg}
                inputError={(this.state.submitted || this.state.login.password.touched) && !this.state.login.password._value}
              />

              {!this.props.loginsuccess && <ListErrors errors={this.props.errors} />}

              <button
                type="submit"
                disabled={this.props.inProgress}
                className="btn btn-lg btn-block swt-button swt-button-gradient--pink-purple">
                Login
                </button>

            </form>
          </div>

        </div>
      </div>

    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
