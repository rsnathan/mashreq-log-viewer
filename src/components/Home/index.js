import React from 'react';
import agent from '../../agent';
import { connect } from 'react-redux';
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
  HOME_PAGE_LOADED_COUNT
} from '../../constants/actionTypes';

import Input from '../shared/input';
import Pagination from '../shared/pagination';

let date = new Date();
let startDate = date.setDate(date.getDate() - 5)
let endDate = date.setDate(date.getDate() + 5)

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token,
  
});

const mapDispatchToProps = dispatch => ({
  onLoad: (currentpage, perpage, startdate, endaate) =>
    dispatch({ type: HOME_PAGE_LOADED, payload: agent.Logs.getAll(currentpage, perpage, new Date(startdate), new Date(endaate)) }),
  onLoadCount: (currentpage, perpage, startdate, endaate) =>
    dispatch({ type: HOME_PAGE_LOADED_COUNT, payload: agent.Logs.getAllCount(currentpage, perpage, new Date(startdate), new Date(endaate)) }),
  onUnload: () =>
    dispatch({ type: HOME_PAGE_UNLOADED })
});

class Home extends React.Component {

  constructor() {
    super();
    this.state = {
      search: {
        startDate: new Date("17/May/2015"),
        endDate: new Date("26/May/2015"),

      },
      pageNo: 0,
      pageLimit: 10,
      showSearch: false
    }
  }

  componentWillMount() {

    this.props.onLoad(this.state.pageNo, this.state.pageLimit, this.state.search.startDate, this.state.search.endDate);
    this.props.onLoadCount(this.state.pageNo, this.state.pageLimit, this.state.search.startDate, this.state.search.endDate);
  }

  componentWillUnmount() {
    this.props.onUnload();
  }
  /**
   * Handle Input change for Controlled Element
   * @param {*} e targer data
   */
  onHandleInput = (e) => {
    const { name, value } = e.target;
    this.setState(prevState => {
      let search = Object.assign({}, this.state.search);
      search[name] = value;
      return { search };
    })
  }
  /**
   * Make Search visible to user
   */
  showSearch = () => {
    this.setState({
      showSearch: true
    })
  }
  /**
   * Search logs function to trigger API
   */
  searchLogs = () => {
    this.setState(prevState => {
      let pageNo = Object.assign({}, this.state.pageNo);
      pageNo = 0;
      return { pageNo };

    }, () => {
      this.props.onLoad(this.state.pageNo, this.state.pageLimit, this.state.search.startDate, this.state.search.endDate);
      this.props.onLoadCount(this.state.pageNo, this.state.pageLimit, this.state.search.startDate, this.state.search.endDate);
    })

  }

  /**
   * Handle Pagination Events
   * @param {*} pageno 
   */
  handlePagination = (pageno) => {
    this.setState(prevState => {
      let pageNo = Object.assign({}, this.state.pageNo);
      pageNo = pageno;
      return { pageNo };

    }, () => {
      this.props.onLoad(this.state.pageNo, this.state.pageLimit, this.state.search.startDate, this.state.search.endDate);
    })
  }
  render() {
    return (
      <div className="home-page">
        <div className="limiter">
          <div className="container-table100 col-md-10 offset-md-1 no-pad">
            <div className="table100 ver1 m-b-110">

              {!this.state.showSearch && <div className="col-md-2 mar-20 ">
                <button
                  type="text"
                  onClick={this.showSearch}
                  className="btn btn-lg btn-block swt-button swt-button-gradient--pink-purple ">
                  Show Search
                </button>
              </div>
              }
              {this.state.showSearch &&
                <div className="col-md-12 pad-right-16">
                  <div className=" row  bg-blue">
                    <div className="col-md-5">
                      <Input
                        type="date"
                        placeholder="Start Date"
                        label="Start Date"
                        name={'startDate'}
                        onChange={this.onHandleInput}
                        value={this.state.search.startDate}
                      />
                    </div>
                    <div className="col-md-5">
                      <Input
                        type="date"
                        placeholder="End Date"
                        label="End Date"
                        name={'endDate'}
                        onChange={this.onHandleInput}
                        value={this.state.search.endDate}
                      />
                    </div>
                    <div className="col-md-2 mar-20">
                      <button
                        type="text"
                        disabled={this.props.inProgress}
                        onClick={this.searchLogs}
                        className="btn btn-lg btn-block swt-button swt-button-gradient--pink-purple">
                        Search
                </button>
                    </div>
                  </div>
                </div>
              }

              <div className="table100-body js-pscroll ps ps--active-y">
                <table>
                  <thead>

                    <tr className=" head">
                      <th>Agent</th>
                      <th>Bytes</th>
                      <th>Referrer</th>
                      <th>Remote IP</th>
                      <th>Remote User</th>
                      <th>Request</th>
                      <th>Response</th>
                      <th>Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.logs && this.props.logs.length > 0 && this.props.logs.map((log, index) => {
                      return (
                        <tr key={index} className=" body">
                          <td>{log.agent}</td>
                          <td>{log.bytes}</td>
                          <td>{log.referrer}</td>
                          <td>{log.remote_ip}</td>
                          <td>{log.remote_user}</td>
                          <td>{log.request}</td>
                          <td>{log.response}</td>
                          <td>{log.time}</td>
                        </tr>
                      );
                    })
                    }
                    {this.props.logs && this.props.logs.length == 0 &&
                      <tr className=" body ">
                        <td colSpan="8" className="text-center">No Result Found</td>
                      </tr>
                    }
                  </tbody>
                </table>
              </div>
            </div>



            <div className="col-md-12 pagination-wrapper">
              <Pagination ref="pagination"
                currentPage={this.state.pageNo == 0 ? 1 : this.state.pageNo}
                handlePaginationAction={this.handlePagination}
                count={this.props.logCount}
                perpage={this.state.pageLimit} />
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
