import React from 'react';

class ListErrors extends React.Component {
  render() {
    const errors = this.props.errors;
    if (errors) {
      return (
        <span className="error-messages">
          {errors}
        </span>
      );
    } else {
      return null;
    }
  }
}

export default ListErrors;
