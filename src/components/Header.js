import React from 'react';
import { Link } from 'react-router-dom';
import Logo from './shared/Logo';
const LoggedOutView = props => {
  if (!props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/login" className="nav-link">
            Sign in
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/register" className="nav-link">
            Sign up
          </Link>
        </li>

      </ul>
    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">
        <li className="nav-item">
          <Link
            to={`#`}
            className="nav-link">
            {props.currentUser}
          </Link>
        </li>
      </ul>
    );
  }

  return null;
};

class Header extends React.Component {
  
  render() {
    console.log("Nathan this is Token", this.props)
    return (
      <nav className="navbar navbar-default navbar-fixed-top bg-white">
        <div className="container">

          <Link to="/" className="navbar-brand">
            <Logo className="header-logo" />
          </Link>
          <LoggedInView currentUser={this.props.currentUser} />
        </div>
      </nav>
    );
  }
}

export default Header;
