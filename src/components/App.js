import agent from '../agent';
import Header from './Header';
import React from 'react';
import { connect } from 'react-redux';
import { APP_LOAD, REDIRECT, CLOSE_MODAL, LOGOUT } from '../constants/actionTypes';
import { Route, Switch } from 'react-router-dom';
import Home from '../components/Home';
import Login from '../components/Login';
import { store } from '../store';
import { push } from 'react-router-redux';


import Modal from 'react-modal';
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    // marginRight: '-50%',
    width: '33%',
    transform: 'translate(-50%, -50%)'
  }
};


const mapStateToProps = state => {
  console.log("State", state)
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo,
    showModal: state.common.showModal,
    msg: state.common.msg,
  }
};

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) =>
    dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
  closeModal: () =>
    dispatch({ type: CLOSE_MODAL }),
  logOut: () =>
    dispatch({ type: LOGOUT }),
  onRedirect: () =>
    dispatch({ type: REDIRECT })
});


class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      // this.context.router.replace(nextProps.redirectTo);
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const token = window.localStorage.getItem('jwt');

    if (token) {
      agent.setToken(token);
      this.props.history.replace('/home')
      this.props.onLoad(token ? agent.Auth.current() : null, token);
    } else {
      this.props.history.replace('/')
      this.props.onLoad(token ? agent.Auth.current() : null, token);
    }


  }
  authHandler = (err, data) => {
    console.log(err, data);
  };

  closeModal = () => {
    window.localStorage.setItem('jwt', '')
    this.props.closeModal();
    this.props.history.replace('/');
    this.props.logOut();
  }

  render() {

    if (this.props.appLoaded) {
      return (
        <React.Fragment>


          <Modal
            isOpen={this.props.showModal}
            // onAfterOpen={afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
          >

            <div className="modal-lg">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                <h4 className="modal-title">Error!</h4>
              </div>
              <div className="modal-body">
                <p>{this.props.msg}</p>
              </div>
              <div className="modal-footer">
                <button type="button" onClick={this.closeModal} className="btn btn-lg btn-block swt-button swt-button-gradient--pink-purple" data-dismiss="modal">Ok</button>
              </div>
            </div>

          </Modal>


          <div>
            {this.props.currentUser && <Header
              appName={this.props.appName}
              currentUser={this.props.currentUser}
            />
            }
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/home" component={Home} />
            </Switch>
          </div>
        </React.Fragment>
      );
    } else {
      return (
        <div>
          <Header
            appName={this.props.appName}
            currentUser={this.props.currentUser} />
        </div>
      );
    }
  }
}

// App.contextTypes = {
//   router: PropTypes.object.isRequired
// };

export default connect(mapStateToProps, mapDispatchToProps)(App);
