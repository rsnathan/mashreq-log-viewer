export const APP_LOAD = 'APP_LOAD';
export const REDIRECT = 'REDIRECT';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const SHOW_MODAL = 'SHOW_MODAL';


export const HOME_PAGE_LOADED = 'HOME_PAGE_LOADED';
export const HOME_PAGE_LOADED_COUNT = 'HOME_PAGE_LOADED_COUNT';
export const HOME_PAGE_UNLOADED = 'HOME_PAGE_UNLOADED';



export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const REGISTER = 'REGISTER';
export const LOGIN_PAGE_UNLOADED = 'LOGIN_PAGE_UNLOADED';
export const REGISTER_PAGE_UNLOADED = 'REGISTER_PAGE_UNLOADED';
export const ASYNC_START = 'ASYNC_START';
export const ASYNC_END = 'ASYNC_END';

