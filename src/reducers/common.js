import { object } from 'prop-types';
import {
  APP_LOAD,
  REDIRECT,
  LOGOUT,
  LOGIN,
  REGISTER,
  HOME_PAGE_UNLOADED,
  LOGIN_PAGE_UNLOADED,
  SHOW_MODAL,
  CLOSE_MODAL
} from '../constants/actionTypes';

const defaultState = {
  appName: 'Mashreq Log App',
  token: null,
  viewChangeCounter: 0
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
        currentUser: action.error ? null : action.payload ? action.payload.name : null,
        id: action.error ? null : action.payload ? action.payload.id : null,
        email: action.error ? null : action.payload ? action.payload.email : null
      };
    case REDIRECT:
      return { ...state, redirectTo: null };
    case LOGOUT:
      return { ...state, redirectTo: '/', token: null, currentUser: null };
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        redirectTo: action.error ? null : '/',
        token: action.error ? null : action.payload.jwt,
        currentUser: action.error ? null : action.payload.name,
        id: action.error ? null : action.payload.id,
        email: action.error ? null : action.payload.email
      };
    case SHOW_MODAL: {
      return {
        ...state,
        showModal: true,
        msg: (typeof action.msg == "object" ? "Something Went Wrong, kinldy contact your system adminstrator" : action.msg)
      }
    }
    case CLOSE_MODAL:
      return {
        ...state,
        showModal: false
      }
    case HOME_PAGE_UNLOADED:
    case LOGIN_PAGE_UNLOADED:
      return { ...state, viewChangeCounter: state.viewChangeCounter + 1 };
    default:
      return state;
  }
};
