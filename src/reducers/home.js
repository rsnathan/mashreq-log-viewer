import { HOME_PAGE_LOADED, HOME_PAGE_UNLOADED ,ASYNC_START , HOME_PAGE_LOADED_COUNT } from '../constants/actionTypes';

export default (state = { logs: [] , logCount : 0 }, action) => {
  switch (action.type) {
    case HOME_PAGE_LOADED: {
      return {
        ...state,
        logs: action.payload.data,
        inProgress : false
      };
    }
    case HOME_PAGE_LOADED_COUNT: {
      return {
        ...state,
        logCount: action.payload.data
      };
    }
    case ASYNC_START:
        return { ...state, inProgress: true };
    case HOME_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
