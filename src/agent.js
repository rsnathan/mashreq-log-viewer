import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost:3000';

const encode = encodeURIComponent;
const responseBody = res => res.body;

let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set('authorization', `${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
  current: () =>
    requests.get('/user'),
  login: (data) => {
    return requests.post('/auth/login', { user: data })
  },
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  save: user =>
    requests.put('/user', { user })
};

const Logs = {
  getAllCount: (currentpage, perpage, startdate, enddate) => requests.get(`/logs/count?startdate=${startdate}&enddate=${enddate}`),
  getAll: (currentpage, perpage, startdate, enddate) => requests.get(`/logs/list?startdate=${startdate}&enddate=${enddate}&${limit(perpage, currentpage)}`)
};


const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;


export default {
  Auth,
  Logs,
  setToken: _token => { token = _token; }
};
